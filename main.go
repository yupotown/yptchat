package main

import (
	"html/template"
	"io"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

type Template struct {
	Templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.Templates.ExecuteTemplate(w, name, data)
}

type TemplateData struct {
	Messages []string
}

func main() {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Renderer = &Template{
		Templates: template.Must(template.ParseGlob("public/views/*.html")),
	}

	data := TemplateData{}

	e.GET("/", func(c echo.Context) error {
		return c.Render(http.StatusOK, "index", data)
	})

	e.POST("/", func(c echo.Context) error {
		message := c.FormValue("message")
		if message != "" {
			data.Messages = append(data.Messages, message)
		}
		return c.Render(http.StatusOK, "index", data)
	})

	e.GET("/clear", func(c echo.Context) error {
		data.Messages = []string{}
		return c.Render(http.StatusOK, "index", data)
	})

	e.Logger.Fatal(e.Start(":12345"))
}
